import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:prueba_vale_room/screens/roomVR.dart';
import 'utils/auth.dart';
import 'screens/rootPages.dart';


void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
  return runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Prueba vale room',
      routes: <String, WidgetBuilder>{
        '/roomVR' : (context) => RoomVR()
      },
      home: RootPages(Auth()),
    )
  );
}
