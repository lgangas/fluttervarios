import 'dart:ui';

import 'package:flutter/material.dart';

class Carousel extends StatefulWidget {
  @override
  _CarouselState createState() => _CarouselState();
}

class _CarouselState extends State<Carousel> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        children: <Widget>[
          // Root for status bar
          Container(
            width: double.infinity,
            height: 20.0,
          ),

          //Cards
          Expanded(
            child: _MyCardFlipper(),
          ),

          // Bottom bar
          Container(
            width: double.infinity,
            height: 50.0,
            color: Colors.grey,
          )

        ],
      ),
    );
  }
}

class _MyCardFlipper extends StatefulWidget {
  @override
  __MyCardFlipperState createState() => __MyCardFlipperState();
}

class __MyCardFlipperState extends State<_MyCardFlipper> with TickerProviderStateMixin {
  double scrollPercent = 0.0;
  Offset startDrag;
  double startDragPercentScroll;
  double finishScrollStart;
  double finishScrollEnd;
  AnimationController finishScrollController;


  @override
  void initState() {
    super.initState();
    finishScrollController = AnimationController(vsync: this,duration: Duration(milliseconds: 150))
    ..addListener((){
      setState(() {
        scrollPercent = lerpDouble(finishScrollStart,finishScrollEnd,finishScrollController.value);
      });
    });
  }


  @override
  void dispose() {
    finishScrollController.dispose();
    super.dispose();
  }

  List<Widget> _buildCards(){
    return [
      _buildCard(0,3,scrollPercent),
      _buildCard(1,3,scrollPercent),
      _buildCard(2,3,scrollPercent),
    ];
  }

  Widget _buildCard(int cardIndex, int cardCount, double scrollPercent){
    final cardScrollPercent = scrollPercent / (1 / cardCount);

    return FractionalTranslation(
      translation: Offset(cardIndex - cardScrollPercent, 0.0),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: _MyCard(),
      )
    );
  }

  _onHorizontalDragStart(DragStartDetails details){
    startDrag = details.globalPosition;
    startDragPercentScroll = scrollPercent;
  }

  _onHorizontalDragUpdate(DragUpdateDetails details){
    final currentDrag = details.globalPosition;
    final dragDistance = currentDrag.dx - startDrag.dx;
    final singleCardDragPercent = dragDistance / context.size.width;
    final numCards = 3;
    setState(() {
      scrollPercent = (startDragPercentScroll + (-singleCardDragPercent / numCards)).clamp(0.0, 1.0 - (1 / numCards));
    });
  }

  _onHorizontalDragEnd(DragEndDetails details){
    final numCards = 3;
    finishScrollStart = scrollPercent;
    finishScrollEnd = (scrollPercent * numCards).round() / numCards;
    finishScrollController.forward(from: 0.0);

    setState(() {
      startDrag = null;
      startDragPercentScroll = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragStart: _onHorizontalDragStart,
      onHorizontalDragUpdate: _onHorizontalDragUpdate,
      onHorizontalDragEnd: _onHorizontalDragEnd,
      behavior: HitTestBehavior.translucent,
      child: Stack(
        children: _buildCards(),
      ),
    );
  }
}



class _MyCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        // Background
        ClipRRect(
          borderRadius: BorderRadius.circular(20.0),
          child: Image.network('https://i.pinimg.com/originals/36/5b/25/365b25b39f5b00111aabd3c1ccce60a9.jpg',fit: BoxFit.fill)
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Favorite Trees'.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 2.0
              ),
            )
          ],
        )
        
        // Content
      ],
    );
  }
}
