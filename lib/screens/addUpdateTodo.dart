import 'package:flutter/material.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:prueba_vale_room/modules/toDo.dart';
import 'package:prueba_vale_room/utils/dbhelper.dart';

class AddUpdateTodo extends StatefulWidget{

  final String _type;
  final _todo;

  AddUpdateTodo(this._type,[this._todo]);

  @override
  State<StatefulWidget> createState() {
    return AddUpdateTodoState();
  }
}

class AddUpdateTodoState extends State<AddUpdateTodo>{

  DateFormat _dateFormat = DateFormat('dd/MM/yyyy');
  Widget _widgetDateButton = Icon(Icons.date_range,color: Colors.white,);

  String _appBarTitle = 'Add Todo';
  String _title = '';
  String _description = '';
  int _optPriority = 2;
  DateTime _date;

  final _keyForm = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    switch(widget._type){
      case 'add':
        addScreen();
        break;
      case 'update':
        updateScreen();
        break;
    }
  }

  void addScreen(){
    setState(() {
    });
  }

  void updateScreen(){
    var todo = widget._todo;
    setState(() {
      _appBarTitle = 'Update Todo';
      _title = todo['title'];
      _description = todo['description'];
      _date = _dateFormat.parse(todo['date']);
      _optPriority = todo['priority'];
      _widgetDateButton = Text(_dateFormat.format(_date),style: TextStyle(color: Colors.white),);
    });
  }

  Future<Null> _selectDate(context) async{
    DateTime dateNow = DateTime.now();
    final DateTime picker = await showDatePicker(context: context,
        initialDate: _date ?? dateNow, firstDate: DateTime(dateNow.year), lastDate: DateTime(dateNow.year + 5));
    if(picker != null){
      setState(() {
        _date = picker;
        _widgetDateButton = Text(_dateFormat.format(_date),style: TextStyle(color: Colors.white),);
      });
    }
  }

  void validateForm(){
    final form = _keyForm.currentState;

    if(form.validate()){
      form.save();
      switch(widget._type){
        case 'add':
          addTodo();
          break;
        case 'update':
          updateTodo();
          break;
      }
    }
  }

  void addTodo(){    
    ToDo todo = ToDo(_title, _dateFormat.format(_date), _optPriority, _description);
    DbHelper().insertTodo(todo).then((newTodo){
      if(newTodo > 0){
        _showDialog('Todo inserted succesfully.').then((dialog)=>Navigator.pop(context,true));
      }
    });
  }

  void updateTodo(){
    int id = widget._todo['id'];
    ToDo todo = ToDo.withId(id,_title, _dateFormat.format(_date), _optPriority, _description);
    DbHelper().updateTodo(todo).then((todoId){
      if(todoId > 0){
        _showDialog('Todo updated succesfully.').then((dialog)=>Navigator.pop(context,true));
      }
    });
  }

  Future<void> _showDialog(text) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context){
        return AlertDialog(
          title: Text(text,),
          actions: <Widget>[
            FlatButton(
              onPressed: ()=> Navigator.of(context).pop(),
              child: Text('Ok'),
            ),
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_appBarTitle),
        actions: <Widget>[
          IconButton(
            onPressed: ()=>validateForm(),
            icon: Icon(Icons.check),
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _keyForm,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              TextFormField(
                initialValue: _title,
                validator: (valid) => valid.isEmpty ? 'There must be a title.' : null,
                decoration: InputDecoration(
                  labelText: 'Title'
                ),
                onSaved: (text) => _title = text,
              ),
              TextFormField(
                initialValue: _description,
                validator: (valid) => valid.isEmpty ? 'There must be a description.' : null,
                decoration: InputDecoration(
                  labelText: 'Description'
                ),
                onSaved: (text) => _description = text,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: DropdownButtonFormField<int>(
                      value: _optPriority,
                      onChanged: (opt){
                        setState(() {
                          _optPriority = opt;
                        });
                      },
                      items: <DropdownMenuItem<int>>[
                        DropdownMenuItem<int>(
                          child: Text('High'),
                          value: 1,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('Mid'),
                          value: 2,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('Low'),
                          value: 3,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: RaisedButton(
                      child: _widgetDateButton,
                      color: Colors.lightGreen,
                      onPressed: ()=>_selectDate(context),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

}