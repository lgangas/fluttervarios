import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:prueba_vale_room/screens/detailPokemon.dart';

class ScrollPokemon extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ScrollPokemonState();
  }
}

class ScrollPokemonState extends State<ScrollPokemon> with SingleTickerProviderStateMixin{

  final _listTilePokemon = <ListTile>[];
  int _start = 0;

  void getPokemonList()async {
    final response = await http.get(
      'https://pokeapi.co/api/v2/pokemon?offset=$_start&limit=20',
      headers: {'Accept' : 'application/json'}
    );
    if(response.statusCode == 200){
      final pkList = json.decode(response.body)['results'].toList();
      pkList.forEach((pk){
        final pkNumber = getPokemonNumber(pk['url']);
        _listTilePokemon.add(
            ListTile(
              onTap:  ()=>rootToPokemonDetail(pkNumber),
              contentPadding: EdgeInsets.symmetric(horizontal: 16),
              leading: CircleAvatar(
                child: Image.network('https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/$pkNumber.png'),
                backgroundColor: Colors.transparent,
              ),
              title: Text(
                pk['name'],
              ),
            ));
      });
      setState(() {});
    }
  }

  rootToPokemonDetail(pkNumber){
    Navigator.push(context,
        MaterialPageRoute(
            builder: (context){
              return DetailPokemon(pkNumber);
            }
        )
    );
  }

  getPokemonNumber(String e) => int.tryParse(e.split('/')[e.split('/').length - 2]);

  buildListViewPokemon(){
    return ListView.builder(

        padding: EdgeInsets.all(16.0),
        itemCount: _listTilePokemon.length + 1,
        itemBuilder: (context,index){

          if(index == 0) {
            return SizedBox(
              height: 240,
            );
          }

          if(index == _listTilePokemon.length -1){
            _start = _start + 20;
            getPokemonList();
          }

          return Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: index == 1 ? BorderRadius.only(topLeft: Radius.circular(20.0),topRight: Radius.circular(20.0)) : null,
            ),
            child: _listTilePokemon[index - 1],
          );
        }
    );
  }

  @override
  void initState() {
    super.initState();

    getPokemonList();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
//    return makeCustomScrollView();
    return Builder(
      builder: (context){
        return Scaffold(
          appBar: AppBar(
            title: Text('Pokemon list'),

          ),
          body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage('assets/img/backgrounds/pokemongo.jpg'),fit: BoxFit.fill,)
            ),
            child: SafeArea(
              child: buildListViewPokemon(),
            ),
          ),
        );
      },
    );

  }

  makeCustomScrollView(){
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage('assets/img/backgrounds/pokemongo.jpg'),fit: BoxFit.fill,)
        ),
        child: CustomScrollView(
          slivers: <Widget>[
//            SliverAppBar(
//              pinned: true,
//              backgroundColor: Colors.red,
////              expandedHeight: 180.0,
////              flexibleSpace: FlexibleSpaceBar(
////                background: Image.asset('assets/img/backgrounds/pokemongo.jpg',fit: BoxFit.fill,)
////              ),
//            ),

            SliverPadding(
              padding: EdgeInsets.all(16.0),
              sliver: SliverList(
                delegate: SliverChildBuilderDelegate((context,index){

                  if(index == _listTilePokemon.length - 6){
                    _start = _start + 20;
                    getPokemonList();
                  }

                  return Material(
                    borderRadius: index == 0 ? BorderRadius.only(topLeft: Radius.circular(20.0),topRight: Radius.circular(20.0)) : null,
                    color: Colors.white,
                    child: _listTilePokemon[index],
                  );
                },childCount: _listTilePokemon.length ),
              ),
            ),
          ],
        ),
      ),
    );
  }

}