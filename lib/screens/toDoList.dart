import 'package:flutter/material.dart';
import 'package:prueba_vale_room/screens/addUpdateTodo.dart';
import 'package:prueba_vale_room/utils/dbhelper.dart';
import 'package:prueba_vale_room/modules/toDo.dart';

class ToDoList extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return ToDoListState();
  }
}

class ToDoListState extends State<ToDoList>{

  final _keyScaffold = GlobalKey<ScaffoldState>();

  DbHelper helper = DbHelper();
  ListView _listViewTodo;

  void getTodoList(){
    helper.getTodos().then(
      (result){
        buildTodoList(result);
      }
    );
  }

  void buildTodoList(result){
    List<Widget> tiles = [];
    result.toList().forEach((todo){
      tiles.add(
        Dismissible(
          key: Key(todo['id'].toString()),
          onDismissed: (direction)=> deleteTodo(todo['id']),
          background: Container(
            color: Colors.red,
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Icon(Icons.delete,color: Colors.white,),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Icon(Icons.delete,color: Colors.white,),
                ),
              ],
            ),
          ),
          child: ListTile(
            onTap: ()=> rootToAddUpdateTodo('update',todo: todo),
            title: Text(todo['title'],style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700),),
            subtitle: Text(todo['description'],style: TextStyle(fontSize: 15.0, color: Colors.black54),),
            leading: CircleAvatar(
              backgroundColor: avatarColor(todo['priority']),
              child: Text(todo['priority'].toString(),style: TextStyle(color: Colors.white),),
            ),
          )
        )
      );
    });
    setState(() {
      _listViewTodo = ListView.builder(
        itemCount: result.length,
        itemBuilder: (context, index){
          return tiles[index];
        }
      );
    });
  }

  void deleteTodo(id){
    DbHelper().deleteTodo(ToDo.onlyId(id))
    .then((response){
      final scaffold = _keyScaffold.currentState;
      scaffold.showSnackBar(SnackBar(content: Text('Todo deleted.')));
    });
  }
  
  Color avatarColor(int number){
    Color color;
    switch(number){
      case 1:
        color = Colors.redAccent;
        break;
      case 2:
        color = Colors.orangeAccent;
        break;
      case 3:
        color = Colors.lightGreen;
        break;
    }
    return color;
  }
  
  void rootToAddUpdateTodo(type,{todo})async{
    bool value = await Navigator.push(context,
      MaterialPageRoute(builder: (context)=> AddUpdateTodo(type,todo))
    ) ?? false ;
    if(value){
      getTodoList();
    }
  }

  @override
  void initState() {
    super.initState();
    getTodoList();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Text('To Do List'),
        actions: <Widget>[
          IconButton(
            onPressed: () => rootToAddUpdateTodo('add'),
            icon: Icon(Icons.add),
          )
        ],
      ),
      body: _listViewTodo,
    );
  }
  
}