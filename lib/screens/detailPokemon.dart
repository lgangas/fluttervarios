import 'dart:math';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/animation.dart';

class DetailPokemon extends StatefulWidget{
  final pokemonNumber;

  DetailPokemon(this.pokemonNumber);

  @override
  State<StatefulWidget> createState() {
    return DetailPokemonState();
  }
}

class DetailPokemonState extends State<DetailPokemon>with SingleTickerProviderStateMixin{

  String _pkName = '';
  var _listTypes = <Widget>[];

  AnimationController _animationController;
  Animation _animationPk;

  @override
  void initState() {
    super.initState();
    animatePokemon();
    getDetailPokemon().then((resp){
      _animationController.forward();
    });
  }

  animatePokemon(){
    _animationController = AnimationController(vsync: this, duration: Duration(seconds: 2));
    _animationPk = Tween(begin: -1.0, end : 0.0).animate(CurvedAnimation(parent: _animationController, curve: Curves.fastOutSlowIn));
  }

  Future<void> getDetailPokemon() async {
    final response = await http.get('https://pokeapi.co/api/v2/pokemon/${widget.pokemonNumber}',
      headers: {
        'Accept' : 'application/json'
      }
    );

    if(response.statusCode == 200){
      final pokemon = json.decode(response.body);
      setState(() {
        _pkName = pokemon['name'];
        _listTypes = getTypes(pokemon);
      });
    }
  }

  getTypes(pokemon){
    var listTypes = <Widget>[];
    pokemon['types'].forEach((type){
      listTypes.add(
          Image(
            image: AssetImage('assets/img/pk_types/${type['type']['name']}.png'),
            width: 80,
            height: 40,
          )
      );
      listTypes.add(SizedBox(width: 10.0,));
    });
    return listTypes;
  }

  @override
  Widget build(BuildContext context) {
    final double width= MediaQuery.of(context).size.width;
    final double height= MediaQuery.of(context).size.height;

    return Material(
      child: ListView(
        children: <Widget>[
          AnimatedBuilder(
            animation: _animationController,
            builder: (context,child){
              return Stack(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width ,
                    decoration: BoxDecoration(
                      color: Colors.lightBlue,
                    ),
                  ),
                  Positioned(
                    top: MediaQuery.of(context).size.height / 2,
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 50,horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('ABOUT THIS POKEMON', style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),),
                          Text('ASDASDASDASDASDASDASDASDASD', style: TextStyle(color: Colors.black54, fontSize: 25,),),
                        ],
                      ),
                      height: MediaQuery.of(context).size.height /2 ,
                      width: MediaQuery.of(context).size.width ,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.0),
                            topRight: Radius.circular(20.0),
                          )
                      ),
                    ),
                  ),
                  Positioned(
                    top: MediaQuery.of(context).size.height / 2,
                    child: Container(
                      height: 35.0 ,
                      width: MediaQuery.of(context).size.width ,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(colors: <Color>[Colors.black54,Colors.white],begin: Alignment.topCenter,end: Alignment.bottomCenter),
    //                      gradient: LinearGradient(colors: <Color>[Colors.black87,Colors.white],begin: Alignment(1,-1.5),end: Alignment(1,1)),
                          color: Colors.black,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.0),
                            topRight: Radius.circular(20.0),
                          )
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: IconButton(
                        icon: Icon(Icons.arrow_back,color: Colors.white,),
                        onPressed: () => Navigator.pop(context)
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 60, left: 25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('NAME:',style: TextStyle(fontSize: 15,color: Colors.white70),),
                        Text(_pkName.toUpperCase(),style: TextStyle(fontSize: 30,color: Colors.white,fontWeight: FontWeight.bold),),
                        SizedBox(height: 30,),
                        Text('TYPE:',style: TextStyle(fontSize: 15,color: Colors.white70),),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: _listTypes,
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: MediaQuery.of(context).size.height / 2.45,
                    right: 10.0,
                    child : Transform(
                      transform: Matrix4.translationValues(0.0 , _animationPk.value * height, 0.0),
                      child:  Container(
                        height: 180,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle
                        ),
                        child: InkWell(
                          onTap: ()=> print('Hola'),
                          child: Image(
                            image: NetworkImage('https://media.redadn.es/poke/i/pokedex/sprites/6/${widget.pokemonNumber}.png'),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 20,
                    bottom: 50,
                    child: Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        color: Colors.deepOrange,
                        shape: BoxShape.circle,
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.black87,
                              offset: Offset(0, 5.0),
                              blurRadius: 5.0
                          )
                        ]
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    bottom: 50,
                    child: Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        color: Colors.deepOrange,
                        shape: BoxShape.circle,
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.black87,
                            offset: Offset(0, 5.0),
                            blurRadius: 5.0
                          )
                        ]
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ],
      )
    );

  }

}
