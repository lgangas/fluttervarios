import 'dart:io';

import 'package:flutter/material.dart';
import 'package:prueba_vale_room/modules/userSingleton.dart';
import 'package:prueba_vale_room/screens/animationPractice.dart';
import 'package:prueba_vale_room/screens/carousel.dart';
import 'package:prueba_vale_room/screens/geolocationVR.dart';
import 'package:prueba_vale_room/screens/homeValeRoom.dart';
import 'package:prueba_vale_room/screens/movingObjects.dart';
import 'package:prueba_vale_room/screens/storageImages.dart';
import 'package:prueba_vale_room/screens/verticalCarousel.dart';
import 'package:prueba_vale_room/utils/auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'notificationMaker.dart';
import 'scrollPokemons.dart';
import 'toDoList.dart';

class MainPage extends StatefulWidget{

  final BaseAuth auth;
  final VoidCallback signedOut;
  MainPage({this.auth,this.signedOut});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MainPageState();
  }
}

class MainPageState extends State<MainPage>{

  bool _existImage = false;
  String _urlImage = '';

  void onSelectedPopUp(option){
    switch(option){
      case '1':
        signOut();
        break;
      case '2':
        rootToPushNotificationMaker();
        break;
      case '3':
        rootToScrollPokemons();
        break;
      case '4':
        rootToToDoList();
        break;
      case '5':
        rootToHomeValeRoom();
        break;
      case '6':
        rootToGeolocationVR();
        break;
      case '7':
        rootToAnimationPractice();
        break;
      case '8':
        rootToMovingObjects();
        break;
      case '9':
        rootToStorageImages();
        break;
      case '10':
        rootToCarousel();
        break;
      case '11':
        rootToVerticalCarousel();
        break;
    }
  }

  @override
  void initState() {
    super.initState();
//    getImageAvatar();
    Auth().currentUser().then((user){
        UserSingleton().uid = user.uid;
    });
  }

  void rootToHomeValeRoom(){
    Navigator.push(context,
      MaterialPageRoute(builder: (ctx){
        return HomeValeRoom();
      })
    );
  }

  void rootToVerticalCarousel(){
    Navigator.push(context,
      MaterialPageRoute(builder: (ctx){
        return VerticalCarousel();
      })
    );
  }

  void rootToCarousel(){
    Navigator.push(context,
      MaterialPageRoute(builder: (ctx){
        return Carousel();
      })
    );
  }
  void rootToStorageImages(){
    Navigator.push(context,
      MaterialPageRoute(builder: (ctx){
        return StorageImages();
      })
    );
  }

  void rootToMovingObjects(){
    Navigator.push(context,
      MaterialPageRoute(builder: (ctx){
        return MovingObjects();
      })
    );
  }

  void rootToAnimationPractice(){
    Navigator.push(context,
      MaterialPageRoute(builder: (ctx){
        return AnimationPractice();
      })
    );
  }

  void rootToGeolocationVR(){
      Navigator.push(context,
        MaterialPageRoute(builder: (ctx){
          return GeolocationVR();
        })
      );
  }

  void rootToToDoList(){
    Navigator.push(context,
      MaterialPageRoute(
          builder: (context){
            return ToDoList();
          }
      )
    );
  }

  getImageAvatar()async{
    StorageReference storageReference = FirebaseStorage.instance.ref().child('myImage.png');
    await storageReference.getDownloadURL().then((url){
      setState(() {
        _urlImage = url;
        print(_urlImage);
        _existImage = true;
      });
    });
  }

  void signOut(){
    widget.auth.signedOut()
    .then((data){
      widget.signedOut();
    });
  }

  void rootToPushNotificationMaker(){
    Navigator.push(context,
      MaterialPageRoute(
        builder: (context){
          return PushNotificationMaker();
        }
      ));
  }

  void rootToScrollPokemons(){
    Navigator.push(context,
        MaterialPageRoute(
            builder: (context){
              return ScrollPokemon();
            }
        ));
  }

  _showImage(image){
    showDialog(context: context,builder: (context){
      return Padding(
        padding: EdgeInsets.symmetric(vertical: 100.0,horizontal: 50),
        child: Hero(
          tag: 'tag-profile-image',
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Image(
              image: image,
              fit: BoxFit.cover,
            ),
          )
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          leading: Padding(
            padding: EdgeInsets.all(8.0),
            child: StreamBuilder(
              stream: FirebaseStorage.instance.ref().child('myImage.png').getDownloadURL().asStream(),
              builder: (context,snapshot){
                if(!snapshot.hasData) return Container();
                ImageProvider image = NetworkImage(snapshot.data);
                return InkWell(
                  onTap: ()=>_showImage(image),
                  child: Hero(
                    tag: 'tag-profile-image',
                    child: CircleAvatar(
                      backgroundImage: image,
                      backgroundColor: Colors.grey,
                    ),
                  ),
                );
              }
            ),
          ),
          title: Text('Main Page'),
          backgroundColor: Colors.lightBlue,
          actions: <Widget>[
            IconButton(
              onPressed: (){},
              icon: PopupMenuButton<String>(
                onSelected: onSelectedPopUp,
                child: Icon(Icons.list),
                itemBuilder: (BuildContext context){
                  return <PopupMenuItem<String>>[
                    PopupMenuItem<String>(child: Text('Carousel'),value: '10',),
                    PopupMenuItem<String>(child: Text('Vertical Carousel'),value: '11',),
                    PopupMenuItem<String>(child: Text('Storages images'),value: '9',),
                    PopupMenuItem<String>(child: Text('Animation practice'),value: '7',),
                    PopupMenuItem<String>(child: Text('Moving objects'),value: '8',),
                    PopupMenuItem<String>(child: Text('Home ValeRoom'),value: '5',),
                    PopupMenuItem<String>(child: Text('Geolocation'),value: '6',),
                    PopupMenuItem<String>(child: Text('Push notification maker'),value: '2',),
                    PopupMenuItem<String>(child: Text('To Do List'),value: '4',),
                    PopupMenuItem<String>(child: Text('Scroll Pokemons'),value: '3',),
                    PopupMenuItem<String>(child: Text('Sign out'),value: '1',),
                  ];
                },
              ),
            ),
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Hello World',
                style: TextStyle(
                  fontStyle: FontStyle.italic,
                  fontSize: 40.0,
                  fontWeight: FontWeight.bold
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

}