import 'package:flutter/material.dart';

class MyRoomsVR extends StatefulWidget {
  @override
  MyRoomsVRState createState() => MyRoomsVRState();
}

class MyRoomsVRState extends State<MyRoomsVR> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.orangeAccent,
      child: Center(
        child: ListView.builder(
          itemCount: 300,
          itemBuilder: (context,index) => Card(child: Text('hola ${index}'),)
        )
      ),
    );
  }
}
