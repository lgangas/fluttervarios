import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';

class StorageImages extends StatefulWidget {
  @override
  _StorageImagesState createState() => _StorageImagesState();
}

class _StorageImagesState extends State<StorageImages> {

  File _image;
  bool _enableButton = true;

  void _uploadImage(context)async{
    if(_image != null){
      _changeEnabledButton();
      final StorageReference storageReference = FirebaseStorage.instance.ref().child('myImage.png');
      final StorageUploadTask task = storageReference.putFile(_image);
      await task.onComplete;
      if(task.isSuccessful){
        _changeEnabledButton();
        Scaffold.of(context).showSnackBar(SnackBar(content: Text('Uploaded image sucessfully')));
      }
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Pick an image')));
    }
  }

  _changeEnabledButton()=>setState(()=>_enableButton = !_enableButton);

  Future _getImage()async{
    File temporalImage = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = temporalImage;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Storage Images'),
      ),
      body: Builder(
        builder: (context){
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: Image(
                    image: _image == null ? AssetImage('assets/img/defaults/default_image.png') : FileImage(_image),
                    height: 180,
                    width: 210,
                    fit: BoxFit.cover,
                  ),
                ),
                RaisedButton(
                  onPressed: _enableButton ? _getImage : null,
                  color: Colors.green,
                  textColor: Colors.white,
                  child: _enableButton ?
                    Text('Get image') :
                    Container(
                      height: 20.0,
                      width: 20.0,
                      child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),strokeWidth: 2.0,),
                    ),
                ),
              ],
            )
          );
        }
      ),
      floatingActionButton: Builder(
        builder: (context){
          return FloatingActionButton(
            onPressed: _enableButton ? ()=>_uploadImage(context) : null,
            child: _enableButton ? Icon(Icons.file_upload) : CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.white)),
          );
        }
      ),
    );
  }
}
