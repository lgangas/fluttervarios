import 'package:flutter/material.dart';
import 'package:prueba_vale_room/screens/login.dart';
import 'package:prueba_vale_room/screens/mainPage.dart';
import 'package:prueba_vale_room/utils/auth.dart';

class RootPages extends StatefulWidget{

  final BaseAuth auth;
  RootPages(this.auth);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RootPagesState();
  }
}

enum AuthState{
  isNotSigned,
  isSigned
}

class RootPagesState extends State<RootPages>{

  AuthState _authState = AuthState.isNotSigned;

  @override
  void initState() {
    super.initState();
    widget.auth.currentUser()
    .then((user){
      setState(() {
        _authState = user == null ? AuthState.isNotSigned : AuthState.isSigned;
      });
    });
  }

  void rootToMainPage(){
    setState(() {
      _authState = AuthState.isSigned;
    });
  }

  void rootToLogin(){
    setState(() {
      _authState = AuthState.isNotSigned;
    });
  }

  @override
  Widget build(BuildContext context) {
    switch(_authState){
      case AuthState.isNotSigned:
        return Login(
          auth : widget.auth,
          signedIn: rootToMainPage,
        );
      case AuthState.isSigned:
        return MainPage(
          auth: widget.auth,
          signedOut: rootToLogin,
        );
    }
    return null;
  }
}