import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart' ;
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GeolocationVR extends StatefulWidget {
  @override
  _GeolocationVRState createState() => _GeolocationVRState();
}

class _GeolocationVRState extends State<GeolocationVR> {

  var _currentLocation ;
  bool _mapToggle = false;

  GoogleMapController _mapController;
  Set<Marker> _listMarkers = Set<Marker>();

  @override
  void initState() {
    super.initState();
    getLocation();
  }


  @override
  void dispose() {
    super.dispose();
  }

  getLocation() {
    Geolocator().getCurrentPosition().then((position){
      setState(() {
        _listMarkers.add(Marker(markerId: MarkerId('current-location') ,position: LatLng(position.latitude, position.longitude)));
        _listMarkers.add(Marker(markerId: MarkerId('current-location-2') ,position: LatLng(position.latitude + 0.0002, position.longitude)));
        _mapToggle = true;
        _currentLocation = position;
      });
    });
  }

  void _onMapCreated(controller){
    setState(() {
      _mapController = controller;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Geolocation'),
      ),
//      body : Container(),
      body: Stack(
        children: <Widget>[
          Center(
            child: Container(
              child: _mapToggle ?
              GoogleMap(
                initialCameraPosition: CameraPosition(
                  target: LatLng(_currentLocation.latitude, _currentLocation.longitude),
                  zoom: 14.4746,
                ),
                onMapCreated: _onMapCreated,
                markers: _listMarkers,
              ) : CircularProgressIndicator(),
            ),
          )
        ],
      ),

    );
  }
}
