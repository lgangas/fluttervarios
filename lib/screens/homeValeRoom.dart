import 'package:flutter/material.dart';
import 'roomVR.dart';
import 'myroomsVR.dart';
import 'profileVR.dart';

class HomeValeRoom extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return HomeValeRoomState();
  }
}

class HomeValeRoomState extends State<HomeValeRoom> with SingleTickerProviderStateMixin{

  Widget currentScreen = RoomVR();

  TabController _tabController;
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this,initialIndex: 0);
    _scrollController = ScrollController();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (context,innerBoxIsScrolled){
          return <Widget>[
            SliverAppBar(
              title: Text('Vale Room'),
              centerTitle: true,
              floating: true,
              pinned: true,
              snap: true,
              elevation: 5.0,
              bottom: TabBar(
                indicatorColor: Colors.lightGreenAccent,
                controller: _tabController,
                labelColor: Colors.white,
                tabs: <Tab>[
                  Tab(icon: Icon(Icons.person, size: 30,)),
                  Tab(icon: Icon(Icons.group, size: 30,)),
                  Tab(icon: Icon(Icons.message, size: 30,)),
                ]
              ),
            )
          ];
        },
        body: TabBarView(
            controller: _tabController,
            children: <Widget>[
              UserProfileVR(),
              RoomVR(),
              MyRoomsVR()
            ]
        )
      ),
    );
  }
}