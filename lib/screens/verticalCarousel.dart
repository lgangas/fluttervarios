import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class VerticalCarousel extends StatefulWidget {
  @override
  _VerticalCarouselState createState() => _VerticalCarouselState();
}

final boxSize = 240.0;

class _VerticalCarouselState extends State<VerticalCarousel> {

  PageController _pageController;
  bool _directionPageView = false;

  final colorList = [
    Colors.redAccent,
    Colors.greenAccent,
    Colors.blueAccent,
    Colors.pinkAccent,
    Colors.purpleAccent
  ];

  _pageListener(){
    setState(() {});
  }

  _onTapContainer(color,index){
    Navigator.push(context,
      MaterialPageRoute(
        builder: (context)=> 
            Hero(
              tag: 'tag-backgroun$index', 
              child: Material(
                child: InkWell(
                  onDoubleTap: ()=>Navigator.pop(context),
                  child: Container(color: color,)
                ),
              ),
            )
      )
    );
  }

  @override
  void initState() {
    _pageController = PageController(viewportFraction: 0.6,)
    ..addListener(_pageListener);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.removeListener(_pageListener);
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: _pageViewBuilder(),
//      floatingActionButton: FloatingActionButton(onPressed: (){
//        _pageController.jumpTo(0.0);
//        setState(() {
//          _directionPageView = !_directionPageView;
//        });
//      },child: Icon(FontAwesomeIcons.columns),),
//      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget _pageViewBuilder(){

    double height = MediaQuery.of(context).size.height * 0.4 ;
    double width = MediaQuery.of(context).size.width * 0.45 ;

    return PageView.builder(
      scrollDirection: Axis.horizontal,
      controller: _pageController,
      itemCount: 5,
      itemBuilder: (context, index){
        double currentPage = 0.0;
        try {
          currentPage =_pageController.page ;
        } catch (_) {}
        final resizeFactor = (1 - (((currentPage - index).abs() * 0.3).clamp(0.0, 1.0)));
        Color color = colorList[index];
        return Align(
            alignment: Alignment.center,
            child: InkWell(
                onTap: ()=>_onTapContainer(color,index),
                child: Hero(
                    tag: 'tag-backgroun$index',
                    child: Container(
                      height: height * resizeFactor,
                      width: width * resizeFactor,
                      decoration: BoxDecoration(
                          color: color,
                          boxShadow: [
                            BoxShadow(offset: Offset(0, 0),blurRadius: 4.0,spreadRadius: 2.0,color: Colors.black54)
                          ]
                      ),
                    )
                )
            )
        );
      },
    );
  }
}
