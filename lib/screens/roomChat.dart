import 'package:flutter/material.dart';
import 'package:prueba_vale_room/modules/userSingleton.dart';
import 'package:prueba_vale_room/utils/firecloudServices.dart';

class RoomChat extends StatefulWidget {

  final String _id;
  RoomChat(this._id);

  @override
  _RoomChatState createState() => _RoomChatState();
}

class _RoomChatState extends State<RoomChat> {

  String _message = '';
  bool _enableSendMessage = false;
  TextEditingController _editingController;

  @override
  void initState() {
    super.initState();
    _editingController = TextEditingController(text: _message);
  }

  void _onChangeMessage(String text){
    if(text.trim().length != 0){
      _message = text.trim();
      _enableSendMessage = true;
    } else {
      _enableSendMessage = false;
    }
    setState(() {});
  }

  void _sendMessage(){
    var item = {
      'roomId' : widget._id,
      'message' : _message,
      'uid' : UserSingleton().uid,
      'send' : DateTime.now()
    };
    FireCloudServices().setMessage(item)
    .then((resp){
      setState(() {
        _message = '';
        _editingController.text = _message;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _editingController.dispose();
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Room'),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage('assets/img/backgrounds/pokemongo.jpg'),fit: BoxFit.fill)
        ),
        child: Stack(
          children: <Widget>[
            Center(
              child: Container(
                color: Colors.transparent,
                child: Column(
                  children: <Widget>[
                    StreamBuilder(
                      stream: FireCloudServices().getConversation(widget._id),
                      builder: (context,snapshot){
                        if(!snapshot.hasData)return CircularProgressIndicator();
                        var uid = UserSingleton().uid;
                        var documents = snapshot.data.documents;

                        return Expanded(
                          child: ListView.builder(itemBuilder: (context,index){
                            var item = documents[index].data;
                            bool position = false;
                            if(uid.compareTo(item['userId']) == 0) position = true;
                            return Align(
                              alignment: position ?Alignment.centerRight : Alignment.centerLeft,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(8.0),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(20.0),
                                          bottomRight: Radius.circular(20.0),
                                          topLeft: Radius.circular(position ? 20.0 : 0.0),
                                          topRight: Radius.circular(position ? 0: 20.0),
                                        )
                                    ),
                                    child: Text(item['message']),
                                  ),
                                  SizedBox(height: 8.0,)
                                ],
                              ),
                            );
                          },
                            reverse: true,
                            itemCount: documents.length,
                            padding: EdgeInsets.all(8.0),
                          ),
                        );
                      },
                    ),
                    SizedBox(
                      height: 55.0,
                    )
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: Container(
                height: 55.0,
                padding: EdgeInsets.all(5.0),
                width: MediaQuery.of(context).size.width,
                color: Colors.transparent,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(left: 16.0,right: 16.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20.0)
                        ),
                        child: TextField(
                          controller: _editingController,
                          onChanged: _onChangeMessage,
                          decoration: InputDecoration(
                            hintText: 'Write something',
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 8.0,),
                    Container(
                      alignment: Alignment.center,
                      child: IconButton(
                        icon: Icon(Icons.send,),
                        onPressed: _enableSendMessage ? _sendMessage : null,
                        color: Colors.white,
                        disabledColor: Colors.white70,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        shape: BoxShape.circle
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
