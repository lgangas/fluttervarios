import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:prueba_vale_room/modules/userSingleton.dart';
import 'package:prueba_vale_room/utils/auth.dart';

class CreateRoom extends StatefulWidget {
  @override
  _CreateRoomState createState() => _CreateRoomState();
}

class _CreateRoomState extends State<CreateRoom> {

  final _keyForm = GlobalKey<FormState>();
  String _name = '';
  int _totalSize = 2;

  void validateForm(){
    var form = _keyForm.currentState;
    if(form.validate()){
      form.save();
      createRoom();
    }
  }

  void createRoom(){
    Firestore.instance.runTransaction((transaction)async{
      var userId = UserSingleton().uid;
      CollectionReference reference = Firestore.instance.collection('room');
      await reference.add({
        'name' : _name,
        'currentSize' : 1,
        'totalSize' : _totalSize,
        'userId' : userId
      });
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'tag-create-room',
      child: Scaffold(
          appBar: AppBar(
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.check),
                onPressed: validateForm
              )
            ],
            title: Text('Create Room'),
          ),
          body: Container(
            padding: EdgeInsets.all(16.0),
            child: Form(
              key: _keyForm,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    TextFormField(
                      onSaved: (text) => _name = text,
                      validator: (valid)=> valid.isEmpty ? 'Room must have a name.': null,
                      decoration: InputDecoration(
                        labelText: 'Name of the room',
                        alignLabelWithHint: true,
                      ),
                    ),
                    SizedBox(height: 30.0,),
                    Text('Capacity',style: TextStyle(fontSize: 16.0,fontWeight: FontWeight.w300),),
                    SizedBox(height: 5.0,),
                    Slider(value: _totalSize.toDouble(),
                      divisions: 30,
                      min: 2.0,
                      max: 32.0,
                      onChanged:(size){
                        setState(() {
                          _totalSize = size.toInt();
                      });
                    }),
                    Align(
                      child: Text(_totalSize.toInt().toString() +'/32',style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.w300),),
                      alignment: Alignment.bottomRight,
                    )
                  ],
                ),
              ),
            ),
          ),
      ),
    );
  }
}
