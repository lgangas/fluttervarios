import 'package:flutter/material.dart';
import 'package:vector_math/vector_math.dart' show radians;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:math';
import 'dart:io';

class MovingObjects extends StatefulWidget {
  @override
  _MovingObjectsState createState() => _MovingObjectsState();
}

class _MovingObjectsState extends State<MovingObjects> with TickerProviderStateMixin {

  AnimationController _animationController;
  Animation<double> _animationSquare;

  final _listAnimationControllerTriangle = <AnimationController>[];
  final _listAnimationTriangle = <Animation>[];

  final _listTriangles = <Widget>[];

  MyClipper _myClipper;
  
  final _colors = [
    Colors.blue,
    Colors.green,
    Colors.yellow,
    Colors.brown
  ];

  var _randomColor;

  final _figures = [
    'square','pentagon','triangle'
  ];

  //moving center box
  double posX = 0.0;
  double posY = 0.0;
  double boxSize = 100.0;


  // getting bigger center box
  AnimationController _animationControllerBox;
  Animation<double> _animationBox;

  @override
  void initState() {
    super.initState();
    _animationControllerBox = AnimationController(vsync: this,duration: Duration(milliseconds: 100));
    _animationBox = Tween(begin: 0.0, end: 1.0).animate(_animationControllerBox);
    _animationBox.addListener((){
      setState(() {
        boxSize+=_animationBox.value;
      });
    });

    _animationController = AnimationController(vsync: this,duration: Duration(seconds: 3));
    _animationSquare = Tween(
      begin: 0.0,
      end: 360.0,
    ).animate(_animationController);
//    fallingFigure();
    _startAnimation();
  }




  @override
  void dispose() {
    _animationController.dispose();
    _animationControllerBox.dispose();
    _listAnimationControllerTriangle.forEach((controller)=>controller.dispose());
    super.dispose();
  }

  _startAnimation() async {
    try {
      _changeFigure();
      await _animationController.forward();
      _animationController.reset();
      _startAnimation();
    } on TickerCanceled {}
  }

  _changeFigure(){
    var randomFigure = _figures[Random().nextInt(3)];
    setState(() {
      _myClipper = MyClipper(randomFigure);
      _randomColor = _colors[Random().nextInt(4)];
    });
  }

  _makeAnimationSettings(){
    final controller = AnimationController(vsync: this, duration: Duration(milliseconds: 2000));
    final animation = Tween(
      begin: -1.0,
      end: 1.0
    ).animate(controller);

    _listAnimationControllerTriangle.add(controller);
    _listAnimationTriangle.add(animation);

    return <String,dynamic>{
      'controller' : controller,
      'animation' : animation
    };
  }

  fallingFigure()async{
    dynamic obj = _makeAnimationSettings();
    var triangle = fallingTriangle(obj);
    setState(() {
      _listTriangles.add(triangle);
    });
    await obj['controller'].forward();
    removeItems();
  }

  removeItems(){
    _listTriangles.removeAt(0);
    _listAnimationTriangle.removeAt(0);
    _listAnimationControllerTriangle.removeAt(0);
  }

  fallingTriangle(obj){
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width - 20.0;
//    var color = _colors[Random().nextInt(4)];
    var _randomX = Random().nextDouble() * width;
    return AnimatedBuilder(
      animation: obj['controller'],
      builder: (context,child){
        return Transform(
          transform: Matrix4.identity()..translate(_randomX,obj['animation'].value * height,0.0),
          child: ClipPath(
            clipper: _myClipper,
            child: AnimatedContainer(
              duration: _animationController.duration,
              height: 20.0,
              width: 20.0,
              color: _randomColor,
            ),
          ),
        );
      },
    );
  }

  center(context){
    setState(() {
      posX = (MediaQuery.of(context).size.width/2) - (boxSize + 20) / 2;
      posY = MediaQuery.of(context).size.height/2 - (boxSize / 2);
    });
  }

  @override
  Widget build(BuildContext context) {
    if(posX == 0.0){
      center(context);
    }
    return Scaffold(
      body: SizedBox.expand(
        child: AnimatedBuilder(
          animation: _animationController,
          builder: (context,child){
            return Stack(
              children: <Widget>[
                Positioned(
                  bottom: 20,
                  left: 20,
                  child: InkWell(
                    onTap: fallingFigure,
                    child: ClipPath(
                      clipper: _myClipper,
                      child: AnimatedContainer(
                        duration: _animationController.duration,
                        height: 40.0,
                        width: 40.0,
                        color: _randomColor,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: Transform.rotate(
                    angle: radians(_animationSquare.value),
                    child: Container(
                      alignment: Alignment.center,
                      height: 140.0,
                      width: 140.0,
                      decoration: BoxDecoration(
                        color: Colors.pinkAccent,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  child: Column(
                    children: _listTriangles,
                  ),
                ),
                Positioned(
                  top: posY,
                  left: posX,
                  child: AnimatedBuilder(
                    animation: _animationControllerBox,
                    builder: (context,child){
                      return GestureDetector(
                        onLongPress: (){
                          _animationControllerBox.repeat();
                        },
                        onLongPressUp: (){
                          _animationControllerBox.reset();
                        },
                        onDoubleTap: (){
                          setState(() {
                            boxSize = 100.0;
                            center(context);
                          });
                        },
                        onVerticalDragUpdate: (DragUpdateDetails value){
                          double deltaY = value.delta.dy;
                          setState(() {
                            posY += deltaY;
                          });
                        },
                        onHorizontalDragUpdate: (DragUpdateDetails value){
                          double deltaX = value.delta.dx;
                          setState(() {
                            posX += deltaX;
                          });
                        },
                        child: Container(
                          height: boxSize,
                          width: boxSize + 20,
                          decoration: BoxDecoration(
                              color: Color(0xffcceeff),
                              borderRadius: BorderRadius.circular(20.0)
                          ),
                        ),
                      );
                    }
                  )
                )
              ],
            );
          }
        ),
      ),
    );
  }
}

class MyClipper extends CustomClipper<Path> {

  String _figureType;
  MyClipper(this._figureType);

  @override
  Path getClip(Size size) {
    final path = Path();
    final w = size.width / 6;
    final h = size.height / 6;

    switch(_figureType){
      case 'square':
        //square
        path.lineTo(w*6, 0.0);
        path.lineTo(w*6, h*6);
        path.lineTo(0.0, h*6);
        break;
      case 'pentagon':
        // pentagon
        path.lineTo(w*4, 0.0);
        path.lineTo(w*6, h*3);
        path.lineTo(w*3, h*6);
        path.lineTo(0.0, h*3);
        path.lineTo(w*2, 0.0);
        break;
      case 'triangle':
        // triangle
        path.lineTo(0.0 , h*6);
        path.lineTo(w*3 , 0.0);
        path.lineTo(w*6, h*6);
        path.lineTo(0.0 , h*6);
        break;
    }

    path.close();
    return path;
  }

  @override
  bool shouldReclip(MyClipper oldClipper) => false;
}

