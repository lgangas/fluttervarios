import 'package:flutter/material.dart';
import 'package:prueba_vale_room/modules/userSingleton.dart';
import 'mainPage.dart';
import 'package:prueba_vale_room/utils/auth.dart';

class Login extends StatefulWidget{
  final BaseAuth auth;
  final VoidCallback signedIn;
  Login({this.auth,this.signedIn});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginState();
  }

}

class LoginState extends State<Login>{

  final _formKey = GlobalKey<FormState>();
  String _email, _password;

  bool validateForm(BuildContext context){
    final form = _formKey.currentState;

    if(form.validate()){
      form.save();
      return true;
    }
    return false;
  }

  void validateAuth(BuildContext context) async{
    if(validateForm(context)){
      try{
        widget.auth.signInWithEmailAndPassword(_email, _password)
        .then((uid){
          widget.signedIn();
        });
      }catch(e){
        print('Error: $e');
      }
    }
  }


  @override
  Widget build(BuildContext context) {

    return Material(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Log in'),
          centerTitle: true,
        ),
        body: Builder(
            builder: (context){
              return Container(
                padding: EdgeInsets.all(16),
                child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        TextFormField(
                          decoration: InputDecoration(
                            suffixIcon: Icon(Icons.email),
                            hintText: 'Email',
                          ),
                          validator: (valid) => valid.isEmpty ? "Email can't be empty." : null,
                          onSaved: (text) => _email = text,
                        ),
                        TextFormField(
                          obscureText: true,
                          decoration: InputDecoration(
                            suffixIcon: Icon(Icons.vpn_key),
                            hintText: 'Password',
                          ),
                          validator: (valid) => valid.isEmpty ? "Password can't be empty." : null,
                          onSaved: (text) => _password = text,
                        ),
                        RaisedButton(
                          color: Colors.green,
                          textColor: Colors.white,
                          child: Text('login'),
                          onPressed: ()=>validateAuth(context),
                        )
                      ],
                    )
                ),
              );
            }
        )
      ),
    );
  }

  void _showToast(BuildContext context, String message)=> Scaffold.of(context).showSnackBar(SnackBar(content: Text(message)));

}
