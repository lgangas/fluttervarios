import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:prueba_vale_room/modules/userSingleton.dart';
import 'package:prueba_vale_room/screens/roomChat.dart';
import 'createRoom.dart';

class RoomVR extends StatefulWidget {
  @override
  RoomVRState createState() => RoomVRState();
}

class RoomVRState extends State<RoomVR> with SingleTickerProviderStateMixin {

  void createNewRoom(){
    Navigator.push(context,
      MaterialPageRoute(builder: (builder) => CreateRoom())
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _rootToRoom(id){
    String userId = UserSingleton().uid;
    Firestore.instance.collection('room/$id/members')
    .where('userId',isEqualTo: userId)
    .getDocuments()
    .then((query){
      if(query.documents.length > 0){
        Navigator.push(context, MaterialPageRoute(builder: (context)=>RoomChat(id)));
      }else{
        print('>>>byeee');
        _showDialog((){
          Firestore.instance.collection('room/$id/members').add({
            'userId' : userId
          }).then((document){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>RoomChat(id)));
          });
        });
      }
    });
  }

  Future<void> _showDialog(VoidCallback callback) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context){
        return AlertDialog(
          title: Text('Do you want to join this room?',),
          actions: <Widget>[
            FlatButton(
              onPressed: (){
                callback();
                Navigator.pop(context);
              },
              child: Text('Yes'),
            ),
            FlatButton(
              onPressed: ()=> Navigator.pop(context),
              child: Text('No'),
            ),
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Center(
          child: Container(
            child: StreamBuilder(
              stream: Firestore.instance.collection('room').snapshots(),
              builder: (context,snapshot){
                if(!snapshot.hasData) return CircularProgressIndicator();
                return ListView.builder(
                  padding: EdgeInsets.all(0.0),
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (builder,index){
                    var data = snapshot.data.documents[index].data;
                    var roomId = snapshot.data.documents[index].documentID;
                    return Container(
                      child: ListTile(
                        onTap: ()=>_rootToRoom(roomId),
                        trailing: Container(
                          padding: EdgeInsets.all(5.0),
                          child: Text('${data['currentSize']}/${data['totalSize']}',style: TextStyle(color: Colors.white),),
                          decoration: BoxDecoration(
                              color: Colors.orangeAccent,
                              borderRadius: BorderRadius.circular(20)
                          ),
                        ),
                        subtitle: StreamBuilder(
                          stream: Firestore.instance.collection('room/$roomId/conversationRoom').orderBy('send',descending: true).snapshots(),
                          builder: (context,snapshot){
                            if(snapshot.hasError) return Text('');
                            if(!snapshot.hasData) return Text('');
                            var documents = snapshot.data.documents;
                            if(documents.length > 0) return Text(documents[0].data['message']);
                            else return Text('');
                          }
                        ),
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage('https://picsum.photos/100/100/?random'),
                        ),
                        title: Text(data['name']),
                      )
                    ) ;
                  }
                );
              },
            ),
          ),
        ),
        Positioned(
          right: 20,
          bottom: 20,
          child: FloatingActionButton(
            heroTag: 'tag-create-room',
            onPressed: createNewRoom,
            child: Icon(Icons.add),
          ),
        ),
      ],
    );
  }
}
