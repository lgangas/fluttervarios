import 'package:flutter/material.dart';
import 'package:vector_math/vector_math.dart' show radians;
import 'dart:math';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AnimationPractice extends StatefulWidget {
  @override
  _AnimationPracticeState createState() => _AnimationPracticeState();
}

class _AnimationPracticeState extends State<AnimationPractice> with SingleTickerProviderStateMixin{

  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 900));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox.expand(
        child: Center(child: RadialAnimation(controller: _animationController,)),
      ),
    );
  }
}

class RadialAnimation extends StatelessWidget {
  final AnimationController controller;
  final Animation<double> scale;
  final Animation<double> translation;
  final Animation<double> rotation; 

  RadialAnimation({Key key, this.controller}) :

    scale = Tween(
      begin: 1.5,
      end: 0.0
    ).animate(CurvedAnimation(
        parent: controller,
        curve: Curves.fastOutSlowIn)),
    translation = Tween(
      begin: 0.0,
      end: 100.0
    ).animate(
      CurvedAnimation(
        parent: controller,
        curve: Interval(0.0, 0.7, curve: Curves.decelerate)
      )
    ),
  rotation = Tween(
    begin: 0.0,
    end: 360.0
  ).animate(
    CurvedAnimation(parent: controller, curve: Curves.decelerate)
  ),

  super(key : key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context,builder){
        return Transform.rotate(
          angle: radians(rotation.value),
          child: Stack(
            children: <Widget>[
              _buildButton(0,color: Colors.redAccent,icon: FontAwesomeIcons.thumbtack),
              _buildButton(45,color: Colors.green,icon: FontAwesomeIcons.key),
              _buildButton(90,color: Colors.pink,icon: FontAwesomeIcons.map),
              _buildButton(135,color: Colors.purple,icon: FontAwesomeIcons.infinity),
              _buildButton(180,color: Colors.grey,icon: FontAwesomeIcons.accessibleIcon),
              _buildButton(225,color: Colors.brown,icon: FontAwesomeIcons.affiliatetheme),
              _buildButton(270,color: Colors.yellow,icon: FontAwesomeIcons.angleLeft),
              _buildButton(315,color: Colors.orange,icon: FontAwesomeIcons.batteryFull),
              Transform.scale(
                scale: scale.value - 1.5,
                child: FloatingActionButton(
                  heroTag: 'open',
                  child: Icon(FontAwesomeIcons.timesCircle),
                  onPressed: _close,
                  backgroundColor: Colors.red,
                ),
              ),
              Transform.scale(
                scale: scale.value,
                child: FloatingActionButton(
                  heroTag: 'circle',
                  child: Icon(FontAwesomeIcons.solidDotCircle),
                  onPressed: _open,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  _buildButton(double angle, {Color color, IconData icon}){
    final double rad = radians(angle);
    return Transform(
      transform: Matrix4.identity()..translate(
          (translation.value) * cos(rad),
          (translation.value) * sin(rad)
      ),
      child: FloatingActionButton(
        heroTag: 'icon-angle$angle',
        child: Icon(icon),
        backgroundColor: color,
      ),
    );
  }

  _open(){
    controller.forward();
  }

  _close(){
    controller.reverse();
  }
}

