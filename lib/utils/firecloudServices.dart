import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

class FireCloudServices {

  getConversation(String id){
//    QuerySnapshot().
    return Firestore.instance
      .collection('room/$id/conversationRoom')
      .orderBy('send',descending: true)
      .snapshots();
  }

  Future<void> setMessage(dynamic item)async{
    Firestore.instance.runTransaction((Transaction transaction)async{
      final CollectionReference conversation = Firestore.instance.collection('room/${item['roomId']}/conversationRoom');
      await conversation.add({
        'message' : item['message'],
        'userId' : item['uid'],
        'send' : item['send']
      });
    });
  }
}