import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';

abstract class BaseAuth {
  Future<String> signInWithEmailAndPassword(String email, String password);
  Future currentUser();
  Future signedOut();
}

class Auth implements BaseAuth{
  @override
  Future<String> signInWithEmailAndPassword(String email, String password)async {
    FirebaseUser user = await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password);
    return user.uid;
  }

  @override
  Future currentUser() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    return user;
  }

  @override
  Future signedOut() async{
    await FirebaseAuth.instance.signOut();
    return null;
  }
}