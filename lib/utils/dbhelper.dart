import 'package:sqflite/sqflite.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'package:prueba_vale_room/modules/toDo.dart';

class DbHelper {

  // Table
  String tblTodo = 'todo';
  // Columns
  String colId = 'id';
  String colTitle = 'title';
  String colDescription = 'description';
  String colPriority = 'priority';
  String colDate = 'date';

  // Make class singleton
  static final DbHelper _dbHelper = DbHelper.internal();
  DbHelper.internal();
  factory DbHelper(){
    return _dbHelper;
  }

  static Database _db;

  Future<Database> get db async {
    if(_db == null){
      _db = await initializeDb();
    }
    return _db;
  }

  Future<Database> initializeDb() async {
    Directory dir = await getApplicationDocumentsDirectory();
    String path = dir.path + 'todos.db';
    var dbTodo = await openDatabase(path, version: 1, onCreate: _createDb);
    return dbTodo;
  }

  void _createDb(Database db, int newVersion) async {
    await db.execute(
       'CREATE TABLE $tblTodo (' +
         '$colId INTEGER PRIMARY KEY, '+
         '$colTitle TEXT, '+
         '$colDescription TEXT, '+
         '$colDate TEXT, '+
         '$colPriority INTEGER '+
       ')'
    );
  }

  Future<int> insertTodo(ToDo todo) async {
    Database db = await this.db;
    var result = await db.insert(tblTodo, todo.toMap());
    return result;
  }

  Future<List> getTodos() async {
    Database db = await this.db;
    var result = await db.rawQuery('select * from $tblTodo order by $colPriority asc');
    return result;
  }

  Future<int> getCount()async{
    Database db = await this.db;
    var result = Sqflite.firstIntValue(
      await db.rawQuery('select count(*) from $tblTodo')
    );
    return result;
  }

  Future<int> updateTodo(ToDo todo) async{
    Database db = await this.db;
    var result = db.update(tblTodo,todo.toMap(),
      where: '$colId = ?',whereArgs: [todo.id]
    );
    return result;
  }

  Future<int> deleteTodo(ToDo todo) async{
    Database db = await this.db;
    var result = db.delete(tblTodo,where : '$colId = ?',whereArgs: [todo.id]);
    return result;
  }



}
