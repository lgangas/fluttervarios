class ToDo {
  int _id;
  String _description;
  String _title;
  String _date;
  int _priority;

  ToDo( this._title, this._date, this._priority,[this._description]); // between [] to make property optional
  ToDo.withId(this._id,this._title, this._date, this._priority,[this._description]);
  ToDo.onlyId(this._id);

  int get id => _id;
  String get title => _title;
  String get description => _description;
  String get date=> _date;
  int get priority => _priority;

  set title(String newTitle) => newTitle.length <= 255 ? _title = newTitle : null;
  set description(String newDescription) => newDescription.length <= 255 ? _description = newDescription : null;
  set date(String newDate) => _date = newDate;
  set priority(int newPriority) => newPriority > 0 && newPriority <= 3 ? _priority = newPriority : null;

  Map<String, dynamic> toMap(){
    var map = Map<String,dynamic>();
    map['title'] = _title;
    map['description'] = _description;
    map['priority'] = _priority;
    map['date'] = _date;
    if(_id != null) map['id'] = _id;
    return map;
  }

  ToDo.fromObject(dynamic o){
    _id = o['id'];
    _title = o['title'];
    _description = o['description'];
    _priority = o['priority'];
    _date = o['date'];
  }

}