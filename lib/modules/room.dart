class Room {
  int _id;
  String _userId;
  String _name;
  int _counter;

  Room.withId(this._id, this._userId, this._name, this._counter);
  Room(this._userId, this._name, this._counter);

  int get id => this._id;
  int get counter => this._counter;
  String get userId => this._userId;
  String get name => this._name;

  set setCounter(int counter) => _counter = counter;
  set setUserId(String userId) => _userId = userId;
  set setName(String name) => _name = name;

}