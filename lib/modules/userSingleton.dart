class UserSingleton{
  String _uid;
  String _email;

  static final UserSingleton _userSingleton = UserSingleton.internal();
  UserSingleton.internal();
  factory UserSingleton()=>_userSingleton;

  void set uid(String uid){
    _uid = uid;
  }
  void set email(String email){
    _email = email;
  }

  String get uid => _uid;
  String get email => _email;

}